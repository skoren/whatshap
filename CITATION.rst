Parts of WhatsHap have been described in different articles. Please choose
an appropriate citation depending on your use case.

If you use WhatsHap as a tool:

    | Marcel Martin, Murray Patterson, Shilpa Garg, Sarah O. Fischer,
      Nadia Pisanti, Gunnar W. Klau, Alexander Schoenhuth, Tobias Marschall.
    | *WhatsHap: fast and accurate read-based phasing*
    | bioRxiv 085050
    | doi: `10.1101/085050 <https://doi.org/10.1101/085050>`_

To refer to the core WhatsHap phasing algorithm:

    | Murray Patterson, Tobias Marschall, Nadia Pisanti, Leo van Iersel,
      Leen Stougie, Gunnar W. Klau, Alexander Schönhuth.
    | *WhatsHap: Weighted Haplotype Assembly for Future-Generation Sequencing Reads*
    | Journal of Computational Biology, 22(6), pp. 498-509, 2015.
    | doi: `10.1089/cmb.2014.0157 <http://dx.doi.org/10.1089/cmb.2014.0157>`_
      (`Get self-archived PDF <https://bioinf.mpi-inf.mpg.de/homepage/publications.php?&account=marschal>`_)

To refer to the pedigree-phasing algorithm and the PedMEC problem:

    | Shilpa Garg, Marcel Martin, Tobias Marschall.
    | *Read-based phasing of related individuals*
    | Bioinformatics 2016; 32 (12): i234-i242.
    | doi: `10.1093/bioinformatics/btw276 <https://doi.org/10.1093/bioinformatics/btw276>`_

A parallelization of the core dynamic programming algorithm (“pWhatsHap”)
has been described in

    | M. Aldinucci, A. Bracciali, T. Marschall, M. Patterson, N. Pisanti, M. Torquati.
    | *High-Performance Haplotype Assembly*
    | Proceedings of the 11th International Meeting on Computational Intelligence
      Methods for Bioinformatics and Biostatistics (CIBB), 245-258, 2015.
    | doi: `10.1007/978-3-319-24462-4_21 <http://dx.doi.org/10.1007/978-3-319-24462-4_21>`_

pWhatsHap is currently not integrated into the main WhatsHap source code. It
is available in
`branch parallel <https://bitbucket.org/whatshap/whatshap/branch/parallel>`_
in the Git repository.
